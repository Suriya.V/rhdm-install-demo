Red Hat Decision Manager Install Demo 
=====================================
Project to automate the installation of this product with simple standalone configuration.


Install on your machine
----------------------------------
1. [Download and unzip.](https://gitlab.com/bpmworkshop/rhdm-install-demo/-/archive/master/rhdm-install-demo-master.zip)

2. Add products to installs directory, see installs/README for details and links.

3. Run 'init.sh' or 'init.bat' file. 'init.bat' must be run with Administrative privileges

4. Login to http://localhost:8080/decision-central  (u:erics / p:redhatdm1!)

5. Kie Servier access configured (u: kieserver / p:kieserver1!)

6. Enjoy installed and configured Red Hat Decision Manager.


Optional - Install Red Hat Decision Manager on OpenShift Container Platform
----------------------------------------------------------------------------
See the following project to install Red Hat Decision Manager in a container on OpenShift
Container Platform:

- [App Dev Cloud with Red Hat Decision Manager Install Demo](https://gitlab.com/redhatdemocentral/rhcs-rhdm-install-demo)


Supporting Articles
-------------------
- [AppDev in Cloud - How to put Red Hat Decision Manager in your Cloud](http://www.schabell.org/2018/11/appdev-in-cloud-how-to-put-red-hat-decision-manager-in-your-cloud.html)

- [Cloud Happiness - How to install OpenShift Container Platform with new images and templates in just minutes](http://www.schabell.org/2018/11/cloud-happiness-how-to-install-openshift-container-platform-with-new-images-templates-in-minutes.html)


Released versions
-----------------
See the tagged releases for the following versions of the product:

- v1.2 - JBoss EAP 7.2 and Red Hat Decisoin Manager 7.3 installed locally.

- v1.1 - JBoss EAP 7.2 and Red Hat Decisoin Manager 7.2 installed locally.

- v1.0 - JBoss EAP 7.1.0 and Red Hat Decisoin Manager 7.1.0 installed locally.

![RHDM Login](https://gitlab.com/bpmworkshop/rhdm-install-demo/raw/master/docs/demo-images/rhdm-login.png)

![RHDM Decision Central](https://gitlab.com/bpmworkshop/rhdm-install-demo/raw/master/docs/demo-images/rhdm-decision-central.png)
